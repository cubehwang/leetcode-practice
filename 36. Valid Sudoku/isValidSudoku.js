/**
 * @param {character[][]} board
 * @return {boolean}
 */
var isValidSudoku = function (board) {
  let rows = checkRows(board);
  //   console.log('ROWS', rows);

  let cols = checkCols(board);
  //   console.log('COLS', cols);

  let grid = checkGrid(board);
  //   console.log('GRID', grid);

  return rows && cols && grid;
};

function checkGrid(board) {
  for (let i = 0; i < 9; i++) {
    let m = {};
    let x0 = ~~(i / 3);
    let y0 = i % 3;
    for (let j = 0; j < 9; j++) {
      let x = x0 * 3 + ~~(j / 3);
      let y = y0 * 3 + (j % 3);
      let v = board[x][y];
      if (v !== '.') {
        if (m[v]) return false;
        m[v] = v;
      }
    }
  }
  return true;
}

function checkRows(board) {
  for (let i = 0; i < 9; i++) {
    let m = {};
    for (let j = 0; j < 9; j++) {
      let v = board[i][j];
      if (v !== '.') {
        if (m[v]) return false;
        m[v] = v;
      }
    }
  }
  return true;
}

function checkCols(board) {
  for (let i = 0; i < 9; i++) {
    let m = {};
    for (let j = 0; j < 9; j++) {
      let v = board[j][i];
      if (v !== '.') {
        if (m[v]) return false;
        m[v] = v;
      }
    }
  }
  return true;
}

module.exports = isValidSudoku;
