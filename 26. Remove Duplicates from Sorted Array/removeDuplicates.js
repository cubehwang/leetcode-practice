/**
 * @param {number[]} nums
 * @return {number}
 */
var removeDuplicates = function (nums) {
  let n = 1;
  let v = nums[0];
  for (let i = 1; i < nums.length; i++) {
    if (nums[i] !== v) {
      v = nums[i];
      nums[n++] = v;
    }
  }
  for (let i = n; i < nums.length; i++) {
    nums[i] = '_';
  }
  return n;
};

module.exports = removeDuplicates;
