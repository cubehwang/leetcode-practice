const index = require('./removeDuplicates');

test('[1,1,2]', () => {
  let nums = [1, 1, 2];
  expect(index(nums)).toBe(2);
  expect(nums).toStrictEqual([1, 2, '_']);
});

test('[0,0,1,1,1,2,2,3,3,4]', () => {
  let nums = [0, 0, 1, 1, 1, 2, 2, 3, 3, 4];
  expect(index(nums)).toBe(5);
  expect(nums).toStrictEqual([0, 1, 2, 3, 4, '_', '_', '_', '_', '_']);
});
