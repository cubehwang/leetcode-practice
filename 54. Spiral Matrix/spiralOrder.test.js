const index = require('./spiralOrder');

test('[[1,2,3],[4,5,6],[7,8,9]]', () => {
  let q = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
  ];
  let a = [1, 2, 3, 6, 9, 8, 7, 4, 5];
  expect(index(q)).toStrictEqual(a);
});

test('[[1,2,3,4],[5,6,7,8],[9,10,11,12]]', () => {
  let q = [
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 10, 11, 12],
  ];
  let a = [1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7];
  expect(index(q)).toStrictEqual(a);
});
