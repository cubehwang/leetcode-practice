/**
 * @param {number[][]} matrix
 * @return {number[]}
 */
var spiralOrder = function (matrix) {
  let boundary = [0, 0, matrix.length - 1, matrix[0].length - 1];
  let direction = 0;
  let piece = [];
  while (!isBoundaryEmpty(boundary)) {
    let p = slice(matrix, boundary, direction++ % 4);
    piece.push(p);
    // log(matrix, boundary);
  }
  //   console.log(piece.flat());
  return piece.flat();
};

function slice(matrix, boundary, direction) {
  let p = [];
  let [i0, j0, i1, j1] = boundary;
  if (direction === 0) {
    // first row
    for (let j = j0; j <= j1; j++) {
      p.push(matrix[i0][j]);
    }
    boundary[0] = boundary[0] + 1;
  }
  if (direction === 1) {
    // last col
    for (let i = i0; i <= i1; i++) {
      p.push(matrix[i][j1]);
    }
    boundary[3] = boundary[3] - 1;
  }
  if (direction === 2) {
    // last row
    for (let j = j1; j >= j0; j--) {
      p.push(matrix[i1][j]);
    }
    boundary[2] = boundary[2] - 1;
  }
  if (direction === 3) {
    // first col
    for (let i = i1; i >= i0; i--) {
      p.push(matrix[i][j0]);
    }
    boundary[1] = boundary[1] + 1;
  }
  return p;
}

function isBoundaryEmpty(boundary) {
  return boundary[0] > boundary[2] || boundary[1] > boundary[3];
}

function isInBoundary(boundary, i, j) {
  return (
    boundary[0] <= i && i <= boundary[2] && boundary[1] <= j && j <= boundary[3]
  );
}

function log(matrix, boundary) {
  let s = '';
  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[i].length; j++) {
      let m = isInBoundary(boundary, i, j) ? matrix[i][j] : '.';
      s += String(m).padStart(4);
    }
    s += '\n';
  }
  console.log(s);
}

module.exports = spiralOrder;
