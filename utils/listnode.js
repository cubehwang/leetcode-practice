class ListNode {
  constructor(val, next) {
    this.val = val;
    this.next = next;
  }
}

function arrayToLinkedList(arr) {
  let h0 = null;
  let h = null;
  for (let a of arr) {
    if (!h) {
      h = new ListNode(a, null);
      h0 = h;
    } else {
      h.next = new ListNode(a, null);
      h = h.next;
    }
  }
  return h0;
}

module.exports = {
  ListNode,
  arrayToLinkedList,
};
