/**
 * @param {number[]} nums
 * @return {number}
 */
var deleteAndEarn = function (nums) {
  const counter = nums.reduce((acc, cur) => {
    acc[cur] = (acc[cur] || 0) + 1;
    return acc;
  }, {});

  return Object.keys(counter)
    .sort((a, b) => {
      if (counter[b] == counter[a]) {
        return b - a;
      } else {
        return counter[b] - counter[a];
      }
    })
    .reduce((s, l) => {
      const n = Number(l);
      const c = counter[l];
      if (c > 0) {
        counter[n] = 0;
        if (counter[n - 1]) counter[n - 1] = 0;
        if (counter[n + 1]) counter[n + 1] = 0;
        return s + n * c;
      } else {
        return s;
      }
    }, 0);
};

module.exports = deleteAndEarn;
