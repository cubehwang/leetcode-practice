const deleteAndEarn = require('./deleteAndEarn');

test('[3,4,2], 6', () => {
  expect(deleteAndEarn([3, 4, 2])).toBe(6);
});

test('[2,2,3,3,3,4], 9', () => {
  expect(deleteAndEarn([2, 2, 3, 3, 3, 4])).toBe(9);
});

test('[8,10,4,9,1,3,5,9,4,10], 37', () => {
  expect(deleteAndEarn([8, 10, 4, 9, 1, 3, 5, 9, 4, 10])).toBe(37);
});

test('[8,3,4,7,6,6,9,2,5,8,2,4,9,5,9,1,5,7,1,4], 61', () => {
  expect(
    deleteAndEarn([8, 3, 4, 7, 6, 6, 9, 2, 5, 8, 2, 4, 9, 5, 9, 1, 5, 7, 1, 4]),
  ).toBe(61);
});
