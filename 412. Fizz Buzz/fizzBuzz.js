/**
 * @param {number} n
 * @return {string[]}
 */
var fizzBuzz = function (n) {
  const r = new Array(n);

  for (let i = 0, a = 0, b = 0; i < n; i++, a++, b++) {
    if (a == 2 && b == 4) {
      a = -1;
      b = -1;
      r[i] = 'FizzBuzz';
    } else if (a == 2) {
      a = -1;
      r[i] = 'Fizz';
    } else if (b == 4) {
      b = -1;
      r[i] = 'Buzz';
    } else {
      r[i] = (i + 1).toString();
    }
  }

  return r;
};

module.exports = fizzBuzz;
