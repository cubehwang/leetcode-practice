const {ListNode, arrayToLinkedList} = require('../utils/listnode');

const index = require('./swapPairs');

test('[]', () => {
  let head = arrayToLinkedList([]);
  expect(index(head)).toStrictEqual(arrayToLinkedList([]));
});

test('[1]', () => {
  let head = arrayToLinkedList([1]);
  expect(index(head)).toStrictEqual(arrayToLinkedList([1]));
});

test('[1,2,3,4]', () => {
  let head = arrayToLinkedList([1, 2, 3, 4]);
  expect(index(head)).toStrictEqual(arrayToLinkedList([2, 1, 4, 3]));
});
