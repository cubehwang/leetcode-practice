/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var swapPairs = function (head) {
  if (!head) return null;

  let s = [[], []];
  let h = head;
  let i = 0;
  let idx;
  while (h) {
    idx = i % 2;
    s[idx].push(h);
    h = h.next;
    i++;
  }

  let h0;
  h = null;
  while (s[0].length > 0 || s[1].length > 0) {
    for (let i = 1; i >= 0; i--) {
      let n = s[i].shift();
      if (n) {
        n.next = null;
        if (h) {
          h.next = n;
          h = n;
        } else {
          h0 = n;
          h = n;
        }
      }
    }
  }

  return h0;
};

module.exports = swapPairs;
