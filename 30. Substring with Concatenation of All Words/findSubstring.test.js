const index = require('./findSubstring');

test('s = "barfoothefoobarman", words = ["foo","bar"]', () => {
  expect(index('barfoothefoobarman', ['foo', 'bar'])).toStrictEqual([0, 9]);
});

test('s = "wordgoodgoodgoodbestword", words = ["word","good","best","word"]', () => {
  expect(
    index('wordgoodgoodgoodbestword', ['word', 'good', 'best', 'word']),
  ).toStrictEqual([]);
});

test('s = "barfoofoobarthefoobarman", words = ["bar","foo","the"]', () => {
  expect(
    index('barfoofoobarthefoobarman', ['bar', 'foo', 'the']),
  ).toStrictEqual([6, 9, 12]);
});

test('s = "wordgoodgoodgoodbestword", words = ["word","good","best","good"]', () => {
  expect(
    index('wordgoodgoodgoodbestword', ['word', 'good', 'best', 'good']),
  ).toStrictEqual([8]);
});

test('s = "ababababab", words = ["ababa","babab"]', () => {
  expect(index('ababababab', ['ababa', 'babab'])).toStrictEqual([0]);
});
