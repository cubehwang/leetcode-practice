/**
 * @param {string} s
 * @param {string[]} words
 * @return {number[]}
 */
var findSubstring = function (s, words) {
  let idx = {};
  for (let w of words) {
    let i = -1;
    while (true) {
      i = s.indexOf(w, i + 1);
      if (i !== -1) {
        idx[i] = w;
        continue;
      }
      break;
    }
  }

  let r = [];
  let ids = Object.keys(idx).map((i) => parseInt(i));
  let l = words.length;
  for (let i = 0; i < ids.length; i++) {
    let s = [ids[i]];
    let j = i + 1;
    while (s.length < words.length && j < ids.length) {
      if (ids[j] - s[s.length - 1] === words[0].length) {
        s.push(ids[j]);
      }
      j++;
    }

    if (s.length !== l) {
      continue;
    }

    let t = s.map((ss) => idx[ss]);
    if (!isMatched(t, words)) {
      continue;
    }

    r.push(ids[i]);
  }

  return r;
};

function isContinuous(arr, words) {
  let l = words[0].length;
  for (let j = 0; j < arr.length - 1; j++) {
    if (arr[j + 1] - arr[j] !== l) {
      return false;
    }
  }
  return true;
}

function isMatched(arr, words) {
  let t0 = arr.sort((a, b) => a.localeCompare(b)).join('');
  let t1 = words.sort((a, b) => a.localeCompare(b)).join('');
  return t0.localeCompare(t1) === 0;
}

module.exports = findSubstring;
