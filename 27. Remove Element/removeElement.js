/**
 * @param {number[]} nums
 * @param {number} val
 * @return {number}
 */
var removeElement = function (nums, val) {
  let n = 0;
  for (let i = 0; i < nums.length; i++) {
    if (nums[i] !== val) {
      nums[n++] = nums[i];
    }
  }
  for (let i = n; i < nums.length; i++) {
    nums[i] = '_';
  }
  return n;
};

module.exports = removeElement;
