const index = require('./removeElement');

test('[3,2,2,3], val = 3', () => {
  let nums = [3, 2, 2, 3];
  expect(index(nums, 3)).toBe(2);
  expect(nums).toStrictEqual([2, 2, '_', '_']);
});

test('[0,1,2,2,3,0,4,2], val = 2', () => {
  let nums = [0, 1, 2, 2, 3, 0, 4, 2];
  expect(index(nums, 2)).toBe(5);
  expect(nums).toStrictEqual([0, 1, 3, 0, 4, '_', '_', '_']);
});
