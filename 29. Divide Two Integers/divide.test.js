const index = require('./divide');

test('dividend = 10, divisor = 3', () => {
  expect(index(10, 3)).toBe(3);
});

test('dividend = 7, divisor = -3', () => {
  expect(index(7, -3)).toBe(-2);
});

test('dividend = 0, divisor = 1', () => {
  expect(index(0, 1)).toBe(0);
});

test('dividend = 1, divisor = 1', () => {
  expect(index(1, 1)).toBe(1);
});

test('dividend = -2147483648, divisor = -1', () => {
  expect(index(-2147483648, -1)).toBe(2147483647);
});
