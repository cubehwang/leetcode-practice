/**
 * @param {number} dividend
 * @param {number} divisor
 * @return {number}
 */
var divide = function (dividend, divisor) {
  let MAX = 2147483647;
  let MIN = -2147483648;

  let n = 0;
  if (divisor === 1) {
    n = dividend;
  } else if (divisor === -1) {
    n = -dividend;
  } else {
    let s0 = 1;
    if (dividend < 0) {
      s0 = -1;
      dividend = -dividend;
    }
    let s1 = 1;
    if (divisor < 0) {
      s1 = -1;
      divisor = -divisor;
    }
    while (dividend >= divisor) {
      n++;
      dividend -= divisor;
    }
    if (s0 !== s1) {
      n = -n;
    }
  }

  if (n > MAX) return MAX;
  if (n < MIN) return MIN;
  return n;
};

module.exports = divide;
