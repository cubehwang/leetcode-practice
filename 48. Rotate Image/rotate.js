/**
 * @param {number[][]} matrix
 * @return {void} Do not return anything, modify matrix in-place instead.
 */
var rotate = function (matrix) {
  let N = matrix.length;

  log(matrix);
  for (let i = 0; i < N; i++) {
    for (let j = 0; j < i; j++) {
      let v = matrix[i][j];
      matrix[i][j] = matrix[j][i];
      matrix[j][i] = v;
    }
  }
  log(matrix);
  for (let i = 0; i < N; i++) {
    for (let j = 0; j < N / 2; j++) {
      let v = matrix[i][j];
      matrix[i][j] = matrix[i][N - j - 1];
      matrix[i][N - j - 1] = v;
    }
  }
  log(matrix);
};

function log(matrix) {
  let s = '';
  let N = matrix.length;
  for (let i = 0; i < N; i++) {
    for (let j = 0; j < N; j++) {
      s += ' ' + String(matrix[i][j]).padStart(3, ' ');
    }
    s += '\n';
  }
  console.log(s);
}

module.exports = rotate;
