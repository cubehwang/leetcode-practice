/**
 * @param {character[][]} board
 * @return {void} Do not return anything, modify board in-place instead.
 */
var solveSudoku = function (board) {
  let B = new Board(board);
  let S = new Solver(B);
  // B.log();
  S.solve();
  // B.log();
};

module.exports = solveSudoku;

let log = false;

class Board {
  constructor(value) {
    this.value = value;
  }

  setValue(x, y, z) {
    this.value[x][y] = `${z + 1}`;
  }

  isFreezed(x, y) {
    return this.value[x][y] !== '.';
  }

  isValueInRow(x, y, z, skipSelf) {
    for (let j = 0; j < 9; j++) {
      if (skipSelf && y === j) continue;
      if (this.value[x][j] == z + 1) return true;
    }
    return false;
  }

  isValueInCol(x, y, z, skipSelf) {
    for (let i = 0; i < 9; i++) {
      if (skipSelf && x === i) continue;
      if (this.value[i][y] == z + 1) return true;
    }
    return false;
  }

  isValueInSquare(x, y, z, skipSelf) {
    let x0 = ~~(x / 3) * 3;
    let y0 = ~~(y / 3) * 3;
    for (let i = x0; i < x0 + 3; i++) {
      for (let j = y0; j < y0 + 3; j++) {
        if (skipSelf && i == x && j == y) continue;
        if (this.value[i][j] == z + 1) return true;
      }
    }
    return false;
  }

  isFinish() {
    for (let i = 0; i < 9; i++) {
      for (let j = 0; j < 9; j++) {
        if (this.value[i][j] === '.') return false;
      }
    }
    return true;
  }

  getValue(x, y) {
    return this.value[x][y];
  }

  clone() {
    let r = [];
    for (let i = 0; i < 9; i++) {
      r[i] = [];
      for (let j = 0; j < 9; j++) {
        r[i][j] = this.value[i][j];
      }
    }
    return new Board(r);
  }

  copyFrom(b) {
    for (let i = 0; i < 9; i++) {
      for (let j = 0; j < 9; j++) {
        this.value[i][j] = b.getValue(i, j);
      }
    }
  }

  log() {
    console.log(this.toString());
  }

  toString() {
    let s = '';
    let hl = ' +-------+-------+-------+\n';
    for (let i = 0; i < 9; i++) {
      if (i % 3 === 0) s += hl;
      for (let j = 0; j < 9; j++) {
        if (j % 3 === 0) s += ' |';
        let c = this.value[i][j];
        if (c instanceof Array) {
          c = String.fromCharCode(64 + c.length);
        }
        s += ' ' + c;
      }
      s += ' |\n';
    }
    s += hl;
    return s;
  }
}

class Solver {
  constructor(board) {
    this.board = board;
    this.value = this.init();
  }

  init() {
    let r = {};
    for (let x = 0; x < 9; x++) {
      r[x] = {};
      for (let y = 0; y < 9; y++) {
        r[x][y] = {};
        for (let z = 0; z < 9; z++) {
          r[x][y][z] = !this.board.isFreezed(x, y);
        }
      }
    }
    return r;
  }

  solve() {
    while (this.next()) {}

    for (let g of this.guess()) {
      let s = new Solver(g);
      let finish = s.solve();
      if (finish) {
        this.board.copyFrom(s.getBoard());
        break;
      }
    }
    
    return this.board.isFinish();
  }

  next() {
    this.update();
    let a = this.answer();
    if (log) this.log();
    return a;
  }

  update() {
    for (let x = 0; x < 9; x++) {
      for (let y = 0; y < 9; y++) {
        for (let z = 0; z < 9; z++) {
          if (this.value[x][y][z]) {
            this.value[x][y][z] = this.isCandidate(x, y, z);
          }
        }
      }
    }
  }

  answer() {
    let answer = false;
    for (let x = 0; x < 9; x++) {
      for (let y = 0; y < 9; y++) {
        let z = this.hasOnlyOneCandidate(x, y);
        if (z) {
          this.board.setValue(x, y, z);
          answer = true;
          if (log) console.log('ONLY ONE', x, y, z);
        }
      }
    }
    for (let x = 0; x < 9; x++) {
      for (let y = 0; y < 9; y++) {
        for (let z = 0; z < 9; z++) {
          if (this.isAnswer(x, y, z)) {
            this.board.setValue(x, y, z);
            answer = true;
            if (log) console.log('ANSWER', x, y, z);
          }
        }
      }
    }
    return answer;
  }

  guess() {
    for (let x = 0; x < 9; x++) {
      for (let y = 0; y < 9; y++) {
        let zs = this.hasTwoCandidates(x, y);
        if (zs) {
          if (log) console.log('GUESS', x, y, zs);
          return zs.map((z) => {
            let b = this.board.clone();
            b.setValue(x, y, z);
            return b;
          });
        }
      }
    }
    return [];
  }

  isCandidate(x, y, z) {
    if (this.board.isFreezed(x, y)) return false;
    if (this.board.isValueInRow(x, y, z, true)) return false;
    if (this.board.isValueInCol(x, y, z, true)) return false;
    if (this.board.isValueInSquare(x, y, z, true)) return false;
    return true;
  }

  hasOnlyOneCandidate(x, y) {
    let zs = this.getCandidateValues(x, y);
    if (zs.length === 1) {
      return zs[0];
    } else {
      return null;
    }
  }

  hasTwoCandidates(x, y) {
    let zs = this.getCandidateValues(x, y);
    if (zs.length === 2) {
      return zs;
    } else {
      return null;
    }
  }

  isAnswer(x, y, z) {
    if (!this.value[x][y][z]) return false;
    if (
      [
        this.isValueInRow(x, y, z, true),
        this.isValueInCol(x, y, z, true),
        this.isValueInSquare(x, y, z, true),
      ].every((e) => e)
    ) {
      return false;
    }
    return true;
  }

  isValueInRow(x, y, z, skipSelf) {
    for (let j = 0; j < 9; j++) {
      if (skipSelf && y === j) continue;
      if (this.value[x][j][z]) {
        return true;
      }
    }
    return false;
  }

  isValueInCol(x, y, z, skipSelf) {
    for (let i = 0; i < 9; i++) {
      if (skipSelf && x === i) continue;
      if (this.value[i][y][z]) {
        return true;
      }
    }
    return false;
  }

  isValueInSquare(x, y, z, skipSelf) {
    let x0 = ~~(x / 3) * 3;
    let y0 = ~~(y / 3) * 3;
    for (let i = x0; i < x0 + 3; i++) {
      for (let j = y0; j < y0 + 3; j++) {
        if (skipSelf && i == x && j == y) continue;
        if (this.value[i][j][z]) {
          return true;
        }
      }
    }
    return false;
  }

  getCandidateValues(x, y) {
    let zs = [];
    for (let z = 0; z < 9; z++) {
      if (this.value[x][y][z]) {
        zs.push(z);
      }
    }
    return zs;
  }

  getBoard() {
    return this.board;
  }

  log() {
    let b = this.board.toString().split('\n');
    let c = this.toString().split('\n');
    let s = '';
    while (b.length > 0 || c.length > 0) {
      s += c.shift() + '\t' + (b.shift() || '') + '\n';
    }
    console.log(s);
  }

  toString() {
    let s = '';
    let hl =
      ' +-------+-------+-------+ +-------+-------+-------+ +-------+-------+-------+\n';
    for (let i = 0; i < 27; i++) {
      if (i % 3 === 0) s += hl;
      let x = ~~(i / 3);
      for (let j = 0; j < 27; j++) {
        if (j % 3 === 0) s += ' |';
        let y = ~~(j / 3);
        let z = (i % 3) * 3 + (j % 3);
        let v = this.value[x][y][z];
        s += ' ' + (v ? z + 1 : ' ');
        if (j % 9 === 8 && j < 26) s += ' |';
      }
      s += ' |\n';
      if (i % 9 === 8 && i < 26) s += hl;
    }
    s += hl;
    return s;
  }
}
