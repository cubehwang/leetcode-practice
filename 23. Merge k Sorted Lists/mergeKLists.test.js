const {ListNode, arrayToLinkedList} = require('../utils/listnode');

const index = require('./mergeKLists');

test('[]', () => {
  let lists = [];
  expect(index(lists)).toBe(null);
});

test('[[]]', () => {
  let lists = [[]].map((arr) => arrayToLinkedList(arr));
  expect(index(lists)).toBe(null);
});

test('[[1,4,5],[1,3,4],[2,6]]', () => {
  let lists = [
    [1, 4, 5],
    [1, 3, 4],
    [2, 6],
  ];
  let linkedList = lists.map((arr) => arrayToLinkedList(arr));
  expect(index(linkedList)).toStrictEqual(
    arrayToLinkedList([1, 1, 2, 3, 4, 4, 5, 6]),
  );
});

test('[[],[1]]', () => {
  let lists = [[], [1]].map((arr) => arrayToLinkedList(arr));
  expect(index(lists)).toStrictEqual(arrayToLinkedList([1]));
});
