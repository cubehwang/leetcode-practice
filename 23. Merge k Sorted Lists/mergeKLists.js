/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode[]} lists
 * @return {ListNode}
 */
var mergeKLists = function (lists) {
  if (lists.length === 0) return null;

  let h0, h;
  do {
    h = pickMinNode(lists, h);
    if (!h0) h0 = h;
  } while (h);
  
  return h0;
};

function pickMinNode(lists, h) {
  let mi = findMinIdx(lists);
  if (mi === -1) return null;
  h = pick(lists, mi, h);
  return h;
}

function findMinIdx(lists) {
  let mv = 99999999;
  let mi = -1;
  for (let i = 0; i < lists.length; i++) {
    if (!lists[i]) continue;
    if (mi === -1 || lists[i].val < mv) {
      mv = lists[i].val;
      mi = i;
    }
  }
  // console.log(
  //   'findMinIdx',
  //   lists.map((l) => l && l.val),
  //   mi,
  // );
  return mi;
}

function pick(lists, i, h) {
  let n = lists[i];
  if (!n) {
    return null;
  }
  if (h) {
    h.next = lists[i];
    lists[i] = lists[i].next;
    h = h.next;
  } else {
    h = lists[i];
    lists[i] = lists[i].next;
  }
  h.next = null;
  return h;
}

module.exports = mergeKLists;
