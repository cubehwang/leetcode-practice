const index = require('./insert');

test('intervals = [[1,3],[6,9]], newInterval = [2,5]', () => {
  let i = [
    [1, 3],
    [6, 9],
  ];
  let n = [2, 5];
  let a = [
    [1, 5],
    [6, 9],
  ];
  expect(index(i, n)).toStrictEqual(a);
});

test('intervals = [[1,2],[3,5],[6,7],[8,10],[12,16]], newInterval = [4,8]', () => {
  let i = [
    [1, 2],
    [3, 5],
    [6, 7],
    [8, 10],
    [12, 16],
  ];
  let n = [4, 8];
  let a = [
    [1, 2],
    [3, 10],
    [12, 16],
  ];
  expect(index(i, n)).toStrictEqual(a);
});

test('intervals = [], newInterval = [5,7]', () => {
  let i = [];
  let n = [5, 7];
  let a = [[5, 7]];
  expect(index(i, n)).toStrictEqual(a);
});

test('intervals = [[1,5]], newInterval = [2,3]', () => {
  let i = [[1, 5]];
  let n = [2, 3];
  let a = [[1, 5]];
  expect(index(i, n)).toStrictEqual(a);
});

test('intervals = [[1,5]], newInterval = [2,7]', () => {
  let i = [[1, 5]];
  let n = [2, 7];
  let a = [[1, 7]];
  expect(index(i, n)).toStrictEqual(a);
});

test('intervals = [[1,5]], newInterval = [6,8]', () => {
  let i = [[1, 5]];
  let n = [6, 8];
  let a = [
    [1, 5],
    [6, 8],
  ];
  expect(index(i, n)).toStrictEqual(a);
});

test('intervals = [[1,5]], newInterval = [0,3]', () => {
  let i = [[1, 5]];
  let n = [0, 3];
  let a = [[0, 5]];
  expect(index(i, n)).toStrictEqual(a);
});

test('intervals = [[0,5],[9,12]], newInterval = [7,16]', () => {
  let i = [
    [0, 5],
    [9, 12],
  ];
  let n = [7, 16];
  let a = [
    [0, 5],
    [7, 16],
  ];
  expect(index(i, n)).toStrictEqual(a);
});

test('intervals = [[3,5],[12,15]], newInterval = [6,6]', () => {
  let i = [
    [3, 5],
    [12, 15],
  ];
  let n = [6, 6];
  let a = [
    [3, 5],
    [6, 6],
    [12, 15],
  ];
  expect(index(i, n)).toStrictEqual(a);
});
