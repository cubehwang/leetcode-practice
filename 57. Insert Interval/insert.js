/**
 * @param {number[][]} intervals
 * @param {number[]} newInterval
 * @return {number[][]}
 */
var insert = function (intervals, newInterval) {
  let its = intervals;
  let ni = newInterval;

  let idx = -1;
  if (its.length === 0) {
    its.push(ni);
    idx = 0;
  }

  if (idx === -1 && ni[0] <= its[0][0]) {
    its.unshift(ni);
    idx = 0;
  }

  if (idx === -1 && ni[0] > its[its.length - 1][0]) {
    its.push(ni);
    idx = its.length - 1;
  }

  if (idx === -1) {
    for (let i = 0; i < its.length - 1; i++) {
      let it0 = its[i];
      let it1 = its[i + 1];
      if (it0[0] < ni[0] && ni[0] <= it1[0]) {
        its.splice(i + 1, 0, ni);
        idx = i + 1;
        break;
      }
    }
  }

  for (let i = idx - 1; i >= 0; i--) {
    let it = its[i];
    if (it[1] < ni[0]) break;
    if (ni[0] <= it[1]) {
      ni[0] = ni[0] < it[0] ? ni[0] : it[0];
      ni[1] = ni[1] > it[1] ? ni[1] : it[1];
      its[i] = null;
    }
  }

  for (let i = idx + 1; i < its.length; i++) {
    let it = its[i];
    if (ni[1] < it[0]) break;
    if (it[0] <= ni[1]) {
      ni[0] = ni[0] < it[0] ? ni[0] : it[0];
      ni[1] = ni[1] > it[1] ? ni[1] : it[1];
      its[i] = null;
    }
  }

  return its.filter((i) => i);
};

module.exports = insert;
