/**
 * @param {string} s
 * @return {number}
 */
var longestValidParentheses = function (s) {
  if (s.length === 0) return 0;
  let ps = findPaired(s);
  let ss = truncateUnpaired(s, ps);
  let availables = ss.split('_').filter((s) => s);
  let maxN = availables.reduce((a, b) => Math.max(a, b.length), 0);
  return maxN;
};

function truncateUnpaired(s, ps) {
  let m = ps.reduce((a, b) => {
    a[b[0]] = true;
    a[b[1]] = true;
    return a;
  }, {});
  let r = '';
  for (let i = 0; i < s.length; i++) {
    if (!m[i]) {
      r += '_';
    } else {
      r += s[i];
    }
  }
  return r;
}

function findPaired(s) {
  let p = [];
  let st = [];
  for (let i = 0; i < s.length; i++) {
    if (s[i] === '(') {
      st.push(i);
    }
    if (s[i] === ')') {
      let j = st.pop();
      if (j !== undefined) {
        p.push([j, i]);
      }
    }
  }
  return p;
}

module.exports = longestValidParentheses;
