const index = require('./longestValidParentheses');

test('s = "(()"', () => {
  expect(index('(()')).toBe(2);
});

test('s = ")()())"', () => {
  expect(index(')()())')).toBe(4);
});

test('s = ""', () => {
  expect(index('')).toBe(0);
});

test('s = "()(()"', () => {
  expect(index('()(()')).toBe(2);
});

test('s = "(())("', () => {
  expect(index('(())(')).toBe(4);
});

test('s = ")()())()()("', () => {
  expect(index(')()())()()(')).toBe(4);
});

test('s = "(()))())("', () => {
  expect(index('(()))())(')).toBe(4);
});

test('s = "((())))))()"', () => {
  expect(index('((())))))()')).toBe(6);
});

test('s = "(()(((()"', () => {
  expect(index('(()(((()')).toBe(2);
});

test('s = "))))((()(("', () => {
  expect(index('))))((()((')).toBe(2);
});
