/**
 * @param {number[]} height
 * @return {number}
 */
var trap = function (height) {
  let peak;
  let A = 0;
  let a;
  while (true) {
    peak = findPeak(height);
    a = fillBetweenPeak(height, peak);
    if (a > 0) {
      A += a;
      continue;
    }
    break;
  }

  return A;
};

function fillBetweenPeak(height, peak) {
  let A = 0;
  for (let p = 0; p < peak.length - 1; p++) {
    let {i: i0, h: h0} = peak[p];
    let {i: i1, h: h1} = peak[p + 1];
    let h = Math.min(h0, h1);
    let w = i1 - i0 - 1;
    let a = h * w;
    for (let i = i0 + 1; i < i1; i++) {
      a -= Math.min(h, height[i]);
      height[i] = Math.max(h, height[i]);
    }
    A += a;
  }
  return A;
}

function findPeak(height) {
  let peak = [];
  peak.push({i: 0, h: height[0]});
  for (let i = 1; i < height.length - 1; i++) {
    let v0 = height[i - 1];
    let v1 = height[i];
    let v2 = height[i + 1];
    if (2 * v1 > v0 + v2) {
      peak.push({i, h: v1});
    }
  }
  peak.push({i: height.length - 1, h: height[height.length - 1]});
  return peak;
}

module.exports = trap;
