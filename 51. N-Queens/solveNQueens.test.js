const index = require('./solveNQueens');

test('1', () => {
  let r = [['Q']];
  expect(index(1)).toStrictEqual(r);
});

test('2', () => {
  let r = [];
  expect(index(2)).toStrictEqual(r);
});

test('3', () => {
  let r = [];
  expect(index(3)).toStrictEqual(r);
});

test('4', () => {
  let r = [
    ['.Q..', '...Q', 'Q...', '..Q.'],
    ['..Q.', 'Q...', '...Q', '.Q..'],
  ];
  expect(index(4)).toStrictEqual(r);
});

test('5', () => {
  let r = [
    ['Q....', '..Q..', '....Q', '.Q...', '...Q.'],
    ['Q....', '...Q.', '.Q...', '....Q', '..Q..'],
    ['.Q...', '...Q.', 'Q....', '..Q..', '....Q'],
    ['.Q...', '....Q', '..Q..', 'Q....', '...Q.'],
    ['..Q..', 'Q....', '...Q.', '.Q...', '....Q'],
    ['..Q..', '....Q', '.Q...', '...Q.', 'Q....'],
    ['...Q.', 'Q....', '..Q..', '....Q', '.Q...'],
    ['...Q.', '.Q...', '....Q', '..Q..', 'Q....'],
    ['....Q', '.Q...', '...Q.', 'Q....', '..Q..'],
    ['....Q', '..Q..', 'Q....', '...Q.', '.Q...'],
  ];
  expect(index(5)).toStrictEqual(r);
});
