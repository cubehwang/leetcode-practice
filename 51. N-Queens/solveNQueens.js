/**
 * @param {number} n
 * @return {string[][]}
 */
var solveNQueens = function (n) {
  return nextRow(n, 0).map((js) => {
    let r = init(n);
    for (let i = 0; i < js.length; i++) {
      r[i][js[i]] = 'Q';
    }
    // log(r);
    return r.map((l) => l.join(''));
  });
};

function nextRow(n, i, js = []) {
  if (i === n) {
    return js;
  }
  let rj = [];
  for (let j = 0; j < n; j++) {
    let m = isHitAnotherQueen(js, j);
    if (m) continue;
    rj.push(nextRow(n, i + 1, [...js, j]));
  }
  if (i < n - 1) rj = rj.flat();
  return rj;
}

function isHitAnotherQueen(js, j) {
  for (let i = 0; i < js.length; i++) {
    let j0 = js[js.length - i - 1];
    if (j - i - 1 === j0 || j + i + 1 === j0 || j === j0) {
      return true;
    }
  }
  return false;
}

function init(n) {
  let r = [];
  for (let i = 0; i < n; i++) {
    r[i] = [];
    for (let j = 0; j < n; j++) {
      r[i][j] = '.';
    }
  }
  return r;
}

function log(r) {
  let s = '';
  for (let i = 0; i < r.length; i++) {
    for (let j = 0; j < r[i].length; j++) {
      s += ' ' + r[i][j];
    }
    s += '\n';
  }
  console.log(s);
}

module.exports = solveNQueens;
