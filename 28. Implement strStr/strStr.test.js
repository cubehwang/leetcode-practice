const index = require('./strStr');

test('haystack = "hello", needle = "ll"', () => {
  expect(index('hello', 'll')).toBe(2);
});

test('haystack = "aaaaa", needle = "bba"', () => {
  expect(index('aaaaa', 'bba')).toBe(-1);
});

test('haystack = "", needle = ""', () => {
  expect(index('', '')).toBe(0);
});
