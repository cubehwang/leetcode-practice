/**
 * @param {string} haystack
 * @param {string} needle
 * @return {number}
 */
var strStr = function (haystack, needle) {

  // return haystack.indexOf(needle);

  if (needle.length === 0) return 0;

  let n = -1;
  for (let i = 0; i < haystack.length; i++) {
    let j = 0;
    for (j = 0; j < needle.length; j++) {
      if (haystack[i + j] !== needle[j]) {
        j = -1;
        break;
      }
    }
    if (j === needle.length) {
      n = i;
      break;
    }
  }
  return n;
};

module.exports = strStr;
