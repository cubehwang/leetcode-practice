/**
 * @param {number[]} nums
 * @return {number}
 */
var jump = function (nums) {
  let r = [];
  let s = 0;
  let i = 0;

  if (nums.length < 2) {
    return 0;
  }

  while (i < nums.length) {
    let n = nums[i];
    if (i + n >= nums.length - 1) {
      s++;
      break;
    }

    let m = 0;
    let mi = -1;
    for (let j = i + 1; j <= n + i; j++) {
      if (m < nums[j] + j) {
        m = nums[j] + j;
        mi = j;
      }
    }

    if (mi !== -1) {
      s++;
      r.push(i);
      i = mi;
    } else {
      i++;
    }
  }

  return s;
};

module.exports = jump;
