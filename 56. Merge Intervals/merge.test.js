const index = require('./merge');

test('[[1,3],[2,6],[8,10],[15,18]]', () => {
  let q = [
    [1, 3],
    [2, 6],
    [8, 10],
    [15, 18],
  ];
  let a = [
    [1, 6],
    [8, 10],
    [15, 18],
  ];
  expect(index(q)).toStrictEqual(a);
});

test('[[1,4],[4,5]]', () => {
  let q = [
    [1, 4],
    [4, 5],
  ];
  let a = [[1, 5]];
  expect(index(q)).toStrictEqual(a);
});

test('[[1,4],[0,0]]', () => {
  let q = [
    [1, 4],
    [0, 0],
  ];
  let a = [
    [0, 0],
    [1, 4],
  ];
  expect(index(q)).toStrictEqual(a);
});
