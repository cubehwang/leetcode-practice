/**
 * @param {number[][]} intervals
 * @return {number[][]}
 */
var merge = function (intervals) {
  let its = intervals.sort((a, b) => a[0] - b[0]);
  for (let i = 0; i < its.length; i++) {
    let it = its[i];
    if (!it) continue;

    for (let j = i + 1; j < its.length; j++) {
      let it1 = its[j];
      if (!it1) continue;
      if (it[1] >= it1[0]) {
        it[1] = Math.max(it[1], it1[1]);
        its[j] = null;
      }
    }
  }
  return its.filter((i) => i);
};

module.exports = merge;
