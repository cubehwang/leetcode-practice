/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} k
 * @return {ListNode}
 */
var reverseKGroup = function (head, k) {
  let s = [];
  for (let i = 0; i < k; i++) {
    s[i] = [];
  }

  let h = head;
  let i = 0;
  let idx;
  while (h) {
    idx = i % k;
    s[idx].push(h);
    h = h.next;
    i++;
  }

  let h0;
  h = null;
  while (s.every((ss) => ss.length > 0)) {
    for (let i = k - 1; i >= 0; i--) {
      let n = s[i].shift();
      if (n) {
        n.next = null;
        if (h) {
          h.next = n;
          h = n;
        } else {
          h0 = n;
          h = n;
        }
      }
    }
  }

  for (let i = 0; i < s.length; i++) {
    let n = s[i].shift();
    if (n) {
      n.next = null;
      if (h) {
        h.next = n;
        h = n;
      } else {
        h0 = n;
        h = n;
      }
    }
  }

  //   console.log(h0);
  return h0;
};

module.exports = reverseKGroup;
