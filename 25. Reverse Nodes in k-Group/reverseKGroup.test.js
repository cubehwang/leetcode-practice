const {ListNode, arrayToLinkedList} = require('../utils/listnode');

const index = require('./reverseKGroup');

test('[1] k=1', () => {
  let head = arrayToLinkedList([1]);
  expect(index(head,1)).toStrictEqual(arrayToLinkedList([1]));
});

test('[1,2,3,4,5] k=1', () => {
  let head = arrayToLinkedList([1, 2, 3, 4, 5]);
  expect(index(head,1)).toStrictEqual(arrayToLinkedList([1, 2, 3, 4, 5]));
});

test('[1,2,3,4,5] k=2', () => {
  let head = arrayToLinkedList([1, 2, 3, 4, 5]);
  expect(index(head,2)).toStrictEqual(arrayToLinkedList([2, 1, 4, 3, 5]));
});

test('[1,2,3,4,5] k=3', () => {
  let head = arrayToLinkedList([1, 2, 3, 4, 5]);
  expect(index(head,3)).toStrictEqual(arrayToLinkedList([3, 2, 1, 4, 5]));
});
